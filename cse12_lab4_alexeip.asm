# variable triangles
.text
.globl  main
main:

# a1 contains address called Address 1
# value at Address 1 in memory is another address 2A
# value at Address 2A is the first byte of the ascii encoding of the first string
	li $t0, 0         #indexing for array
	la $t1, ($a0)     #counter for how many times to loop
	li $v0, 4
	la $sp, ($a1)   # after this line, $sp stores Address 1
	#end of initialization
	la $a0, args
	syscall
			
progArgs: #print all program arguments
	beqz $t1, startint
	
	lw $a0, ($sp) # after this line, $a0 stores Address 2A
	lw $t5, ($a0)
	sw $t5, myArray($t0) # Add the argument to the array
		addi $t0, $t0, 4 # increment the index
	syscall
	
	la $a0, space
	syscall
	
	addi $sp, $sp, 4
	subi $t1, $t1, 1
	j progArgs

startint:
	
	la $a0, ints
	syscall
	la $t3, ($t0) # the last index of the array containing an element
	addi $t3, $t3, -4
	li $t0, 0
	li $v0, 1

intvalues:
	li $v0, 1
	bgt $t0, $t3, maxvalue
	la $a0, myArray($t0)
		addi $t0, $t0, 4
	syscall
	
	la $a0, space
	li $v0, 4
	syscall
	
	j intvalues

maxvalue:
	li $v0, 4
	la $a0, max
	syscall

end:
	la $v0, 10
	syscall
.data
space: .asciiz " "
args: .asciiz "Program arguments:\n"
ints: .asciiz "\n\nInteger values:\n"
max: .asciiz "\n\nMaximum Value:\n"
myArray: .align 2 
		 .space 128
